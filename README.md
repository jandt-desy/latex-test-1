# Latex Test Project

* The How-to was taken from here: https://gitlab.hzdr.de/examples/latex-paper/

### Build artifact: Browse and Download

* Browse lated built pdf at:  
  https://gitlab.hzdr.de/jandt-desy/latex-test-1/-/jobs/artifacts/master/file/main.pdf?job=compile_pdf
* Download latest built pdf at:  
  https://gitlab.hzdr.de/jandt-desy/latex-test-1/-/jobs/artifacts/master/raw/main.pdf?job=compile_pdf
